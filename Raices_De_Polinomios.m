function varargout = Raices_De_Polinomios(varargin)
% RAICES_DE_POLINOMIOS MATLAB code for Raices_De_Polinomios.fig
%      RAICES_DE_POLINOMIOS, by itself, creates a new RAICES_DE_POLINOMIOS or raises the existing
%      singleton*.
%
%      H = RAICES_DE_POLINOMIOS returns the handle to a new RAICES_DE_POLINOMIOS or the handle to
%      the existing singleton*.
%
%      RAICES_DE_POLINOMIOS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in RAICES_DE_POLINOMIOS.M with the given input arguments.
%
%      RAICES_DE_POLINOMIOS('Property','Value',...) creates a new RAICES_DE_POLINOMIOS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Raices_De_Polinomios_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Raices_De_Polinomios_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Raices_De_Polinomios

% Last Modified by GUIDE v2.5 25-Apr-2019 11:14:52

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Raices_De_Polinomios_OpeningFcn, ...
                   'gui_OutputFcn',  @Raices_De_Polinomios_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Raices_De_Polinomios is made visible.
function Raices_De_Polinomios_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Raices_De_Polinomios (see VARARGIN)

% Choose default command line output for Raices_De_Polinomios
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes Raices_De_Polinomios wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Raices_De_Polinomios_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in btn_graficar.
function btn_graficar_Callback(hObject, eventdata, handles)
    f=get(handles.txt_funcion,'string');
    syms x;
    ezplot(handles.axes1,f);
    grid on
    hold on
    disp('Fin graficador'); 

function txt_funcion_Callback(hObject, eventdata, handles)
% hObject    handle to txt_funcion (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txt_funcion as text
%        str2double(get(hObject,'String')) returns contents of txt_funcion as a double


% --- Executes during object creation, after setting all properties.
function txt_funcion_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txt_funcion (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in pmenu.
function pmenu_Callback(hObject, eventdata, handles)
contenido = get(hObject,'String');
a = get(hObject,'Value');
texto = contenido(a);

switch cell2mat(texto)
    case 'CUADRATICA (ECUACION CUADRATICA)'
          set(handles.lbl_titulo,'String',texto);
          activarCuadratica(handles);
                  
    case 'CUBICA (METODO DE TARTAGLIA)'
        desactivarCuadratica(handles);
        set(handles.lbl_titulo,'String',texto);
    case 'CUARTICA (METODO DE FERRARI)'
        desactivarCuadratica(handles);
        set(handles.lblcuadratica,'Visible','Off');
        set(handles.lbl_titulo,'String',texto);
    case 'SELECCIONAR'
        desactivarCuadratica(handles);
        set(handles.lblcuadratica,'Visible','Off');
        texto = ' ';
        set(handles.lbl_titulo,'String',texto);
end
 
function activarCuadratica(handles)
          funcion_por_defecto = 'x^2+3*x+10'; 
          set(handles.txt_funcion,'String',funcion_por_defecto);
          set(handles.lblcuadratica, 'Visible', 'on');
          set(handles.A, 'Visible', 'on');
          set(handles.B, 'Visible', 'on');
          set(handles.C, 'Visible', 'on');
          set(handles.editA, 'Visible', 'on');
          set(handles.editB, 'Visible', 'on');
          set(handles.editC, 'Visible', 'on');     
          set(handles.btnCalcular, 'Visible', 'on');
function desactivarCuadratica(handles)
          set(handles.lblcuadratica, 'Visible', 'off');
          set(handles.A, 'Visible', 'off');
          set(handles.B, 'Visible', 'off');
          set(handles.C, 'Visible', 'off');
          set(handles.editA, 'Visible', 'off');
          set(handles.editB, 'Visible', 'off');
          set(handles.editC, 'Visible', 'off');
          set(handles.btnCalcular, 'Visible', 'off');
          set(handles.textX1, 'Visible', 'off');
          set(handles.textX2, 'Visible', 'off');
          set(handles.rptX1, 'Visible', 'off');
          set(handles.rptX2, 'Visible', 'off');
          
function pmenu_CreateFcn(hObject, eventdata, handles)


if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editA_Callback(hObject, eventdata, handles)

function editA_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editB_Callback(hObject, eventdata, handles)

function editB_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editC_Callback(hObject, eventdata, handles)

function editC_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function btnCalcular_Callback(hObject, eventdata, handles)
  set(handles.textX1, 'Visible', 'on');
  set(handles.textX2, 'Visible', 'on');
  set(handles.rptX1, 'Visible', 'on');
  set(handles.rptX2, 'Visible', 'on');
  a=str2double(get(handles.editA, 'String'));
  b=str2double(get(handles.editB, 'String'));
  c=str2double(get(handles.editC, 'String'));
  d=b^2-4*a*c;
    if d>0
    x1=(-b+sqrt(d))/(2*a);
    x2=(-b-sqrt(d))/(2*a);
    disp('valor positivo de la raiz')
    elseif d==0
     x1=-b/(2*a);
     x2=-b/(2*a);
    disp('la raiz vale 0')
    else 
    x1=(-b+1i*sqrt(-d))/(2*a);
    x2=(-b-1i*sqrt(-d))/(2*a);
    disp('la raiz es negativa')
    end
    set(handles.rptX1,'string',num2str(x1));
    set(handles.rptX2, 'string',num2str(x2));
    disp('valores de la ecuaci�n de segundo grado:')
    disp(x1);
    disp(x2);
    %disp(class(x1));
    
